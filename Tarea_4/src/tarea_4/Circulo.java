package tarea_4;
public class Circulo extends Formas{
    private double radio;
    public Circulo(double radio){
        this.setRadio(radio);
        this.setColor("Azul");
    } 
    public String dibujar(){
        return "Circulo";
    }
    public double calcularRadio(){
        double result=Math.PI*Math.pow(radio, 2);
        return result;
    }
        public double getRadio() {
        return radio;
    }
    public void setRadio(double radio) {
        this.radio = radio;
    }
    
}
