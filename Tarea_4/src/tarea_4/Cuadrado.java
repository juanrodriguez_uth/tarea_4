package tarea_4;
public class Cuadrado extends Formas{
    private double area;

    public Cuadrado() {
        this.setColor("MOrado");
    
    }
    public String dibujar(){
       return "Cuadrado";
        }
    public double calcularArea(double l){
        area=l*l;
        return area;
    }

    public double getArea() {
        return area;
    }
    public void setArea(double area) {
        this.area = area;
    }
}
