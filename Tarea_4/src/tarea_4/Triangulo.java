package tarea_4;
public class Triangulo extends Formas{
    private double area;
    public Triangulo(double angulo){
        this.setAngulo(angulo);
        this.setColor("Negro");
    }
    public String dibujar(){
        return "Triangulo";
    }
    public double calcularArea(double b, double a){
        area=(b*a)/2;
        return area;
    }
    public double getAngulo() {
        return area;
    }
    public void setAngulo(double angulo) {
        this.area = angulo;
    }
}
