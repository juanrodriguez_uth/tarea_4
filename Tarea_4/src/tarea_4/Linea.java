package tarea_4;
public class Linea extends Formas{
    private int largo;
    public Linea(int largo){
       this.largo=largo;
       this.setColor("Blanco");
    }
    public String dibujar(){
        return "Linea";
    }
    public int getLargo() {
        return largo;
    }
    public void setLargo(int largo) {
        this.largo = largo;
    }
}
